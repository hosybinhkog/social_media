import {connectDB} from "@libs/mongodb/mongoose";
import User from "@libs/models/user.model";
import {EmailAddressJSON} from "@clerk/types";

export interface CreateUserDto {
  id: string,
  first_name: string,
  last_name: string,
  image_url: string,
  email_addresses: EmailAddressJSON[] | any,
  username: string | null
}


export const createOrUpdateUser = async ({
                                           email_addresses,
                                           id,
                                           first_name,
                                           last_name,
                                           image_url,
                                           username
                                         }: CreateUserDto) => {
  try {
    await connectDB()

    const user = await User.findByIdAndUpdate({
      clerkId: id,
    }, {
      $set: {
        firstName: first_name,
        lastName: last_name,
        username,
        email: email_addresses[0].email_address,
        profilePhoto: image_url
      }
    }, {upsert: true, new: true})

    await user.save();

    return user
  } catch (e) {
    console.log(e)
  }
}

export const deleteUser = async (id: string | undefined) => {
  try {
    await connectDB()
    await User.findByIdAndDelete(id)
    return true
  } catch (e) {
    console.log(e)
  }
}