import mongoose from 'mongoose'

const userScheme = new mongoose.Schema({
  clerkId: {
    type: String,
    required: true
  },
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  profilePhoto: {
    type: String,
    required: true
  },
  posts: {
    type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Post'}],
    default: []
  },
  savedPosts: {
    type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Post'}],
    default: []
  },
  likedPosts: {
    type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Post'}],
    default: []
  },
  followers: {
    type: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    default: []
  },
  followings: {
    type: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    default: []
  }
}, {timestamps: true})

const User = mongoose.models.User || mongoose.model("User", userScheme)

export default User