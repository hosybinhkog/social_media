import mongoose from "mongoose";

let isConnect = false


export const connectDB = async () => {
  mongoose.set('strictQuery', true)

  if (isConnect) {
    console.log("isConnected mongoose")
    return
  }

  try {
    await mongoose.connect(process.env.MONGODB_URL as string, {
      dbName: 'social_app',
      // @ts-ignore
      useNewUrlParser: true, useUnifiedTopology: true
    })

    isConnect = true

    console.log("mongodb is connected")
  } catch (e) {
    console.error(e)
  }
}