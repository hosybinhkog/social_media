'use client'

import {useState} from "react";
import {Add, Logout, Search} from "@mui/icons-material";
import {useRouter} from "next/navigation";
import {SignedIn, SignOutButton} from "@clerk/nextjs";
import Link from "next/link";
import Image from "next/image";

export default function TopNavBar(): JSX.Element {
  const router = useRouter()
  const [search, setSearch] = useState<string>('')

  const handleClickSearch = () => {

  }

  return (
    <div className={'flex justify-between items-center mt-6'}>
      <div className="relative ">
        <input type="text" className={"search-bar"} placeholder={"Search posts,people..."} value={search}
               onChange={e => setSearch(e.target.value)}/>
        <Search className={"search-icon"} onClick={handleClickSearch}/>
      </div>
      <button className={"create-post-btn"} onClick={() => router.push('/create-post')}><Add/>Create a Post</button>

      <div className="flex gap-3 ">
        <SignedIn>
          <SignOutButton>
            <div className="flex cursor-pointer md:hidden items-center">
              <Logout sx={{color: 'white', fontSize: '32px'}}/>
            </div>
          </SignOutButton>
        </SignedIn>

        <Link href={'/'}>
          <Image src={'/assets/sybinh.jpeg'} alt={"image profile"} width={50} height={0}
                 className={"rounded-full h-[50px] md:hidden"}/>
        </Link>
      </div>


    </div>
  )
}