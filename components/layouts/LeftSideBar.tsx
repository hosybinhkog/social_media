import Link from "next/link";
import Image from "next/image";
import Menu from "@components/layouts/Menu";
import {UserButton} from "@clerk/nextjs";
import {dark} from "@clerk/themes";

export default function LeftSideBar() {
  return (
    <div
      className={"h-screen flex flex-col sticky left-0 top-0 overflow-auto px-10 py-6 gap-6 max-md:hidden custom-scrollbar"}>
      <Link href={'/'}>
        <Image src={'/assets/logo.png'} alt={"Logo"} width={200} height={200}/>
      </Link>
      <div className="flex flex-col gap-2">
        <div className="flex flex-col gap-2 items-center text-light-1">
          <Link href={"/"}>
            <Image src={'/assets/sybinh.jpeg'} alt={"avatar"} className={"rounded-full w-[50px] h-[50px] object-cover"}
                   width={50} height={50}/>
          </Link>
          <p className={'text-small-bold'}>
            Ho Sy Binh
          </p>
        </div>
        <div className="flex justify-between text-light-1">
          <div className="flex flex-col items-center">
            <p className="text-base-bold">1</p>
            <p className="text-tiny-medium">Posts</p>
          </div>
          <div className="flex flex-col items-center">
            <p className="text-base-bold">1</p>
            <p className="text-tiny-medium">Posts</p>
          </div>
          <div className="flex flex-col items-center">
            <p className="text-base-bold">1</p>
            <p className="text-tiny-medium">Posts</p>
          </div>
        </div>
        <hr/>
        <Menu/>
        <hr/>
        <div className={'flex gap-4 items-center'}>
          <UserButton appearance={{baseTheme: dark}} afterSignOutUrl="/sign-in"/>
          <p className="text-body-bold text-light-1">Manage Account</p>
        </div>

      </div>
    </div>
  )
}