'use client'
import {ReactNode} from "react";
import TopNavBar from "@components/layouts/TopNavbar";
import {usePathname} from "next/navigation";
import {pageTitles} from "@constansts/index";


export default function MainContainer({children}: { children: ReactNode }): JSX.Element {
  const currentPath = usePathname();

  const regex = /^\/([^\/]+)/;
  const firstPath = currentPath.match(regex)
    // @ts-ignore
    ? currentPath.match(regex)[0]
    : currentPath;

  // Get title of current path
  const title = pageTitles.find((page) => page.url === firstPath)?.title || "";


  return (
    <section className={'flex flex-col flex-1 max-w-3xl px-4 md:px-10 lg:px-4 xl:px-20'}>
      <TopNavBar/>
      <div className="mt-6 mb-20">
        <h1 className={"mb-5 text-heading2-bold max-sm:text-heading3-bold capitalize text-light-1"}>title</h1>
        <div className={"h-screen overflow-y-hidden custom-scrollbar"}>
          {children}
        </div>
      </div>
      {children}
    </section>
  )
}