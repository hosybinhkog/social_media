'use client'

import {usePathname} from "next/navigation";
import {sidebarLinks} from "@constansts/index";
import Link from "next/link";

export default function Menu(): JSX.Element {
  const pathName = usePathname()

  console.log(pathName)

  return (
    <div className={'flex flex-col gap-2'}>
      {
        sidebarLinks.map((sidebarLink) => {
          const isActive = pathName === sidebarLink.route

          return (
            <Link href={sidebarLink.route} key={sidebarLink.label}
                  className={`flex gap-4 justify-start rounded-lg py-2 px-4 ${isActive && 'bg-purple-1'}`}>
              {sidebarLink.icon}
              <p className={'text-light-1'}>
                {sidebarLink.label}
              </p>
            </Link>
          )
        })
      }
    </div>
  )
}