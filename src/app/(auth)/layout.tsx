import {Inter} from "next/font/google";
import {ClerkProvider} from "@clerk/nextjs";

import "../globals.css";
import {ReactNode} from "react";

export const metadata = {
  title: "Auth",
  description: "Next 14 Social Media App",
};

const inter = Inter({subsets: ["latin"]});

export default function RootLayout({children}: { children: ReactNode }) {
  return (
    <ClerkProvider>
      <html lang="en">
      <body className={`${inter.className} bg-purple-2 flex items-center justify-center`}>{children}</body>
      </html>
    </ClerkProvider>
  );
}